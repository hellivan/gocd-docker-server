FROM mhart/alpine-node:6.3.0

ENV base /srv/go-server
WORKDIR ${base}
EXPOSE 8153 8154
CMD ["./server.sh"]

RUN apk update &&\
        apk add docker git openssh-client subversion mercurial openjdk8-jre bash
        

ENV rev 3819
ENV version 16.7.0

RUN mkdir /etc/go /var/log/go-server /var/lib/go-server
VOLUME ["/etc/go", "/var/log/go-server", "/var/lib/go-server"]

ADD ./go-server-config /etc/default/go-server

#ADD ./go-server-${version}-${rev}.zip /tmp/go-server.zip
ADD https://download.go.cd/binaries/${version}-${rev}/generic/go-server-${version}-${rev}.zip /tmp/go-server.zip

RUN cd /tmp &&\
    unzip ./go-server.zip &&\
    rm -rf ${base} &&\
    mv ./go-server-${version} ${base} &&\
    chmod 700 ${base}/server.sh &&\
    rm -rf /tmp/*


